// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OtusHomeworkGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class OTUSHOMEWORK_API AOtusHomeworkGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
