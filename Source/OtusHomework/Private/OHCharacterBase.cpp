// Fill out your copyright notice in the Description page of Project Settings.


#include "OHCharacterBase.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"


// Sets default values
AOHCharacterBase::AOHCharacterBase() {
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComponent =  CreateDefaultSubobject<USpringArmComponent>("SpringArmComponent");
	SpringArmComponent->SetupAttachment(GetRootComponent());
	SpringArmComponent->TargetArmLength = 600.f;
	SpringArmComponent->bUsePawnControlRotation = true;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	CameraComponent->SetupAttachment(SpringArmComponent);
}

// Called when the game starts or when spawned
void AOHCharacterBase::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void AOHCharacterBase::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AOHCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AOHCharacterBase::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AOHCharacterBase::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &AOHCharacterBase::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AOHCharacterBase::AddControllerPitchInput);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AOHCharacterBase::Jump);
}

void AOHCharacterBase::MoveForward(float Value) {
	if (Value == 0.f) return;

	AddMovementInput(GetActorForwardVector(), Value);
}

void AOHCharacterBase::MoveRight(float Value) {
	if (Value == 0.f) return;
    AddMovementInput(GetActorRightVector(), Value);
}

float AOHCharacterBase::GetMovementDirection() const {
	if (GetVelocity().IsZero()) return 0.f;

	const auto VelocityNormal = GetVelocity().GetSafeNormal();
	const auto AngelBetween = FMath::Acos(FVector::DotProduct(GetActorForwardVector(), VelocityNormal));
	const auto CrossProduct = FVector::CrossProduct(GetActorForwardVector(), VelocityNormal);

	return FMath::RadiansToDegrees(AngelBetween) * FMath::Sign(CrossProduct.Z);
}